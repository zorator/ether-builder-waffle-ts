import { ethers, waffle } from "@nomiclabs/buidler";
import chai from "chai";
import { deployContract, solidity } from "ethereum-waffle";

import CounterArtifact from "../artifacts/Counter.json";
import { Counter } from "../typechain/Counter"

chai.use(solidity);
const { expect } = chai;

describe("Counter", () => {
    // 1
    const provider = waffle.provider;
    let [wallet] = provider.getWallets();

    // 2
    // let [wallet] = await ethers.getSigners() as Wallet[]

    let counter: Counter;

    beforeEach(async () => {
        counter = await deployContract(wallet, CounterArtifact) as Counter;
        const initialCount = await counter.count();

        // 4
        expect(initialCount).to.eq(0);
        expect(counter.address).to.properAddress;
    });

    // 5
    describe("count up", async () => {
        it("should count up", async () => {
            await counter.countUp();
            let count = await counter.count();
            expect(count).to.eq(1);
        });
    });

    describe("count down", async () => {
        // 6
        it("should fail", async () => {
            await expect(counter.countDown())
                .to.be.reverted;
        });

        it("should count down", async () => {
            await counter.countUp();

            await counter.countDown();
            const count = await counter.count();
            expect(count).to.eq(0);
        });
    });
});